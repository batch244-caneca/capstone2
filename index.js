const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes"); 

const app = express();

mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.g9tzuwc.mongodb.net/b244_e-Commerce?retryWrites=true&w=majority",
{
		useNewUrlParser : true,
		useUnifiedTopology : true
});


mongoose.connection.once('open', () =>
	console.log('Now connected to the MongoDB Atlas.')
);

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

app.use("/users", userRoutes);
app.use("/products", productRoutes);


app.listen(process.env.PORT || 4000, () =>{
	console.log(`API is now online at port ${process.env.PORT || 4000}.`);
});
