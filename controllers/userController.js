const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product");

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {
		if (result.length > 0) {
			console.log("Duplicate email found.")
			return true;
			
		} else {
			return false;
		}
	})
};

// User registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if (error) {
			console.log("Unable to register user.")
			return false;
		} else {
			console.log("Successfully registered.")
			return true;
		}
	})
}



// User authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}

			} else {
				console.log("Please check your password and try again.")
				return false
			}
		}
	})
};


// Non-admin user checkout

module.exports.checkOut = async (data) => {

	let isOrderUpdated = await Product.findById(data.productId);
	isOrderUpdated.orders.push({userId: data.userId});

	let isUserUpdated = await User.findById(data.userId);

	let totalAmount = isOrderUpdated.price * data.quantity;

	const checkOutProduct = {
            products: [
            	{
	                productName : isOrderUpdated.name,
	                quantity : data.quantity,
	        	}
 			],
            totalAmount : totalAmount
        };

    isUserUpdated.orders.push(checkOutProduct);
    console.log(checkOutProduct)
    console.log(isUserUpdated)
    console.log(isOrderUpdated)
	
	if (isUserUpdated && isOrderUpdated) {
		isOrderUpdated.save();
		isUserUpdated.save();
		return true
	} else {
		return false
	}
};	


//user details
module.exports.getProfile = (reqBody) => {

	return User.findById(reqBody.userId).then(result => {

		result.password = "";

		return result;
	})
};
