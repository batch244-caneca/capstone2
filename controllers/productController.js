const Product = require("../models/Product");

//create product
module.exports.createProduct = (data) => {
	let newProduct = new Product ({
		name: data.product.name,
		description: data.product.description,
		price: data.product.price
	});

	return newProduct.save().then((product, error) => {
			if (error) {
				return false
			} else {
				console.log("Product successfully added.")
				return true;
			}
		})
	};


//all products

module.exports.getAllProducts = () => {

	return Product.find({}).then(result => {
		return result;
	})
}


// Retrieving all active products
module.exports.getAllActive = () => {

	return Product.find({isActive:true}).then(result => {

		return result;
	})
};


//single product
module.exports.getProduct = (reqParams) => {
	
	return Product.findById(reqParams.productId).then(result => {

		return result;
	})
};


// update product
module.exports.updateProduct = (reqParams, reqBody) => {
	

	return Product.findByIdAndUpdate(reqParams.productId, reqBody).then((product, error) => {

		if (error) {
			console.log("Failed to update product.")
			return false

		} else {
			return true
		}
	})
};


// archive a product
module.exports.archiveProduct = (reqParams, reqBody) => {

	let updateActiveField = {
		isActive : reqBody.isActive
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if(error){
			return false
		} else {
			return true
		}
	})
};
