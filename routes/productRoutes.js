const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth")

//creating a product
router.post("/", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)

	if (data.isAdmin){
		productController.createProduct(data).then(resultFromController => res.send(resultFromController));

	} else {
		res.send(false) 
		console.log("Unauthorized user. Failed to create product.")
	}
});


//all products
router.get("/allProducts", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});


//all active products
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});


//single product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId)
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
}) ;


//update a product
router.put("/:productId", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);

	if (data.isAdmin) {
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
		console.log("Failed to update product")
	}
})

//archive a product
router.patch("/:productId/archive", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);

	if (data.isAdmin) {
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
		console.log("Failed to archive product")
	}
});



module.exports = router;
