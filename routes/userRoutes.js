const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

//
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send (resultFromController));
});

//user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


//user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


//user checkout
router.post("/checkOut", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	let data = {
		userId : userData.id,
		productId : req.body.productId,
		quantity: req.body.quantity
	};


	if (userData.isAdmin == false){		
	userController.checkOut(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("Checkout unsuccessful.")
		console.log("Check out unsuccessful.")
	}
});


//user details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

module.exports = router;
